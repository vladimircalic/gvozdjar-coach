const express = require("express");
const router = express.Router();
const WorkoutSchedule = require("../models/workoutScheduleSchema");
const WorkoutRegistration = require("../models/workoutRegistrationSchema");
const { accessTokenCheck, refreshTokenCheck } = require("../middlewares/jwt");
const { adminCheck } = require("../middlewares/admin");
const { checkUserIsVerified } = require("../middlewares/user");
const moment = require("moment");
require("moment/locale/sr");

router.get("/workout-schedule/:type", async (req, res, next) => {
  try {
    const workoutSchedule = await WorkoutSchedule.findOne({ workoutType: req.params.type });
    const registrations = await WorkoutRegistration.find({ workoutType: req.params.type });
    const date = new Date();

    var currentDay = moment(date).day();
    var sortedSchedule = workoutSchedule.schedule.slice(currentDay - 1, 7);
    sortedSchedule.push(...workoutSchedule.schedule.slice(0, currentDay - 1));
    sortedSchedule.forEach((day, index) => {
      day.date = moment(date).add(index, "days").format("Do MMM");
    });
    sortedSchedule.map((day) => {
      day.classes.map((cl) => {
        const atendants = registrations.filter(
          (registration) => registration.trainingClass === cl.time && registration.trainingDate === day.date
        ).length;
        return (cl.attendants = atendants);
      });
      return day;
    });

    workoutSchedule.schedule = sortedSchedule;

    res.status(200).send(workoutSchedule);
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.post("/workout-registration", accessTokenCheck, checkUserIsVerified, async (req, res, next) => {
  try {
    const users = await WorkoutRegistration.find({
      workoutType: req.body.workoutType,
      trainingDate: req.body.trainingDate,
      trainingDay: req.body.trainingDay,
      trainingClass: req.body.trainingClass,
    });
    const userRegistration = await WorkoutRegistration.findOne({
      trainingDate: req.body.trainingDate,
      userId: req.body.userId,
      workoutType: req.body.workoutType,
    });
    const date = new Date();
    if (users.length >= 12) res.status(400).send({ error: "Termin je popunjen, odaberite drugi termin." });
    else if (userRegistration) res.status(400).send({ error: "Možete se registrovati samo za jedan termin dnevno" });
    else if (
      req.body.trainingDate === moment(date).format("Do MMM") &&
      moment(req.body.trainingClass, "h:mma") < moment(moment(date).format("LT"), "h:mma")
    )
      res.status(400).send({ error: "Registracije za odabrani termin su završene" });
    else {
      const workoutRegistration = new WorkoutRegistration(req.body);
      await workoutRegistration.save();
      res.status(200).send({ message: "Trening je zakazan, vidimo se!" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.post("/workout-info", accessTokenCheck, adminCheck, async (req, res, next) => {
  try {
    const users = await WorkoutRegistration.find({
      workoutType: req.body.workoutType,
      trainingDate: req.body.trainingDate,
      trainingDay: req.body.trainingDay,
      trainingClass: req.body.trainingClass,
    });
    res.status(200).send(users);
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.delete("/remove-attendant/:id", accessTokenCheck, adminCheck, async (req, res, next) => {
  try {
    const user = await WorkoutRegistration.findOne({ _id: req.params.id });
    if (!user) res.status(404).send({ error: "Vežbač nije registrovan" });
    else {
      user.remove();
      res.status(200).send({ message: "Vežbač uklonjen" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.get("/user-workouts", accessTokenCheck, async (req, res, next) => {
  try {
    const date = moment(new Date()).format("Do MMM");
    const userWorkouts = await WorkoutRegistration.find({ userId: res.token.userId });
    const filteredWorkouts = userWorkouts.filter(
      (workout) =>
        moment(moment(workout.trainingDate, "Do MMM")).isAfter(moment(date, "Do MMM")) ||
        (workout.trainingDate === moment(date).format("Do MMM") &&
          moment(moment(workout.trainingClass, "h:mma")).isAfter(moment(moment(date).format("LT"), "h:mma")))
    );
    res.status(200).send(filteredWorkouts);
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.post("/cancel-registration", accessTokenCheck, async (req, res, next) => {
  try {
    if (res.token.userId !== req.body.userId) res.status(400).send({ error: "Unauthorized" });
    else {
      const workoutRegistration = await WorkoutRegistration.findOne({
        userId: req.body.userId,
        workoutType: req.body.workoutType,
        trainingDate: req.body.trainingDate,
        trainingClass: req.body.trainingClass,
      });
      if (!workoutRegistration) res.status(404).send({ error: "Registracija ne postoji" });
      else {
        await workoutRegistration.remove();
        res.status(200).send({ message: "Registracija za trening poništena" });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

module.exports = router;
