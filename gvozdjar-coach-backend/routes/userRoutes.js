const express = require("express");
const router = express.Router();
const User = require("../models/userSchema");
const Admin = require("../models/adminSchema");
const WorkoutRegistration = require("../models/workoutRegistrationSchema");
const UserToken = require("../models/userTokenSchema");
const bcrypt = require("bcrypt");
const { accessTokenCheck, refreshTokenCheck, signAccessToken, signRefreshToken } = require("../middlewares/jwt");
const { adminCheck } = require("../middlewares/admin");
const { checkUserExists } = require("../middlewares/user");

//Routes

router.post("/admin-login", async (req, res, next) => {
  try {
    const user = await Admin.findOne({ userName: req.body.userName });

    if (user) {
      if (user.password === req.body.password) {
        const refreshToken = signRefreshToken(user);
        const accessToken = signAccessToken(user);

        const adminToken = new UserToken({
          userId: user._id,
          refreshToken: refreshToken,
          admin: true,
        });

        await adminToken.save();

        res.status(200).send({ message: "Welcome Coachella!", refreshToken: refreshToken, accessToken: accessToken });
      } else res.status(400).send({ error: "Password incorrect!" });
    } else res.status(400).send({ error: "User not found!" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.delete("/admin-logout", refreshTokenCheck, adminCheck, async (req, res, next) => {
  await res.userToken.remove();
  res.status(200).send({ message: "Logged out" });
});

router.get("/all-users", accessTokenCheck, adminCheck, async (req, res, next) => {
  try {
    const users = await User.find();

    res.status(200).send(users);
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.put("/verify-user", accessTokenCheck, adminCheck, async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.body.userId });

    if (req.body.program === "CROSSFIT") user.crossfitActive = !user.crossfitActive;
    if (req.body.program === "FITNESS") user.fitnessActive = !user.fitnessActive;

    await user.save();
    res.sendStatus(200);
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.delete("/delete-user/:id", accessTokenCheck, adminCheck, async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.params.id });
    if (!user) res.status(404).send({ error: "Odabran korisnik ne postoji" });
    else {
      await WorkoutRegistration.deleteMany({ userId: user._id });
      await user.delete();
      res.status(200).send({ message: "Korisnik je obrisan" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.post("/user-register", checkUserExists, async (req, res, next) => {
  try {
    const cryptedPassword = await bcrypt.hash(req.body.password, 10);

    const user = new User({
      email: req.body.email,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      password: cryptedPassword,
      phoneNumber: req.body.phoneNumber,
      crossfitActive: false,
      fitnessActive: false,
    });
    await user.save();
    res.status(201).send({ message: "Korisnik registrovan!" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.post("/user-login", async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) res.status(404).send({ error: "Korisnik ne postoji" });
    else {
      if (await bcrypt.compare(req.body.password, user.password)) {
        const accessToken = signAccessToken(user);
        const refreshToken = signRefreshToken(user);

        const userToken = new UserToken({
          userId: user._id,
          refreshToken: refreshToken,
        });
        await userToken.save();

        res.status(200).send({ message: "Korisnik prijavljen", accessToken: accessToken, refreshToken: refreshToken });
      } else res.status(400).send({ error: "Lozinka netačna" });
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.delete("/user-logout", refreshTokenCheck, async (req, res, next) => {
  try {
    await res.userToken.remove();
    res.status(200).send({ message: "Korisnik odjavljen" });
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.get("/refreshToken", refreshTokenCheck, async (req, res, next) => {
  try {
    let user;
    if (res.userToken.admin) user = await Admin.findOne({ email: res.token.email });
    else user = await User.findOne({ email: res.token.email });
    if (!user) res.status(404).send({ error: "Korisnik ne postoji" });
    else {
      const newAccessToken = signAccessToken(user);
      res.status(200).send(newAccessToken);
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

router.get("/refresh-user-info", accessTokenCheck, async (req, res, next) => {
  try {
    const user = User.findOne({ _id: res.token.userId });
    console.log(user);
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
});

module.exports = router;
