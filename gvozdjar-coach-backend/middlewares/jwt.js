const jwt = require("jsonwebtoken");
const secret = "jwt-secret";
const UserToken = require("../models/userTokenSchema");

const signAccessToken = (user) => {
  const payload = {
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    phoneNumber: user.phoneNumber,
    userId: user._id,
    admin: user.admin ? true : false,
    crossfitActive: user.crossfitActive,
    fitnessActive: user.fitnessActive,
  };
  const accessToken = jwt.sign(payload, secret, { expiresIn: "15m" });
  return accessToken;
};

const signRefreshToken = (user) => {
  const payload = {
    email: user.email,
    firstName: user.firstName,
    lastName: user.lastName,
    phoneNumber: user.phoneNumber,
    userId: user._id,
    admin: user.admin ? true : false,
  };
  const refreshToken = jwt.sign(payload, secret, { expiresIn: "1y" });
  return refreshToken;
};

const accessTokenCheck = (req, res, next) => {
  const accessToken = req.headers.accesstoken;
  if (!accessToken) res.status(401).send({ error: "No access token provided" });
  else {
    jwt.verify(accessToken, secret, (err, decoded) => {
      if (err) res.status(401).send({ error: "Invalid access token" });
      else {
        res.token = decoded;
        next();
      }
    });
  }
};

const refreshTokenCheck = (req, res, next) => {
  const refreshToken = req.headers.refreshtoken;
  if (!refreshToken) res.status(401).send({ error: "No refresh token provided" });
  else {
    jwt.verify(refreshToken, secret, async (err, decoded) => {
      if (err) res.status(401).send({ error: "Invalid refresh token" });
      else {
        const userToken = await UserToken.findOne({ userId: decoded.userId, refreshToken: req.headers.refreshtoken });
        if (!userToken) res.status(401).send({ error: "User not logged in" });
        else {
          res.userToken = userToken;
          res.token = decoded;
          next();
        }
      }
    });
  }
};

module.exports = { signAccessToken, signRefreshToken, accessTokenCheck, refreshTokenCheck };
