const User = require("../models/userSchema");

const checkUserExists = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user) res.status(400).send({ error: "Korisnik sa datom Email adresom već postoji" });
    else next();
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
};

const checkUserIsVerified = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: res.token.userId });
    if (!user) res.status(400).send({ error: "Korisnik nije registrovan" });
    else {
      switch (req.body.workoutType) {
        case "CROSSFIT":
          if (user.crossfitActive) next();
          else res.status(403).send({ error: "Korisnik nije verifikovan za odabran program" });
          break;
        case "FITNESS":
          if (user.fitnessActive) next();
          else res.status(403).send({ error: "Korisnik nije verifikovan za odabran program" });
          break;
        default:
          res.status(404).send({ error: "Odabran program ne postoji" });
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
};

module.exports = { checkUserExists, checkUserIsVerified };
