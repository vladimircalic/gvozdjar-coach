const Admin = require("../models/adminSchema");

const adminCheck = async (req, res, next) => {
  try {
    const user = await Admin.findOne({ _id: res.token.userId });

    if (!user) res.status(404).send("Admin not found");
    else {
      if (!user.admin) res.status(401).send({ message: "Nisi coachela bajo" });
      else {
        res.admin = user;
        next();
      }
    }
  } catch (err) {
    console.log(err);
    res.status(500).send({ error: "Server error" });
  }
};

module.exports = { adminCheck };
