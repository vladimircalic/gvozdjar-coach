const mongoose = require("mongoose");

const workoutRegistrationSchema = new mongoose.Schema({
  workoutType: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  trainingDate: {
    type: String,
    required: true,
  },
  trainingDay: {
    type: String,
    required: true,
  },
  trainingClass: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("WorkoutRegistration", workoutRegistrationSchema);
