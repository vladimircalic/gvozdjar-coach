const mongoose = require("mongoose");

const workoutScheduleSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true,
  },
  schedule: {
    type: Array,
    required: true,
  },
});

module.exports = mongoose.model("workoutSchedule", workoutScheduleSchema);
