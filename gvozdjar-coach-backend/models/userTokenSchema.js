const mongoose = require("mongoose");

const userTokenSchema = mongoose.Schema({
  userId: {
    required: true,
    type: String,
  },
  refreshToken: {
    required: true,
    type: String,
  },
  admin: {
    type: Boolean,
    required: false,
  },
});

module.exports = mongoose.model("UserToken", userTokenSchema);
