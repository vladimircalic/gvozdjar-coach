const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes");
const workoutRoutes = require("./routes/workoutsRoutes");
const path = require("path");

//Database
mongoose.connect("mongodb+srv://noliferop:svesuiste1@gymer-app.zscig.mongodb.net/Gvozdjar-Coach", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.log(error));
db.once("open", () => console.log("Connected to Gvožđar Coach database"));

//Port
app.listen(port, () => console.log(`Gvožđar Coach listening on port ${port}...`));

//App
app.use(express.json());
app.use(express.static(path.join(__dirname, "../gvozdjar-coach-frontend/build")));

app.use("/api", userRoutes);
app.use("/api", workoutRoutes);
app.get("/**", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "../gvozdjar-coach-frontend/build/") });
});
