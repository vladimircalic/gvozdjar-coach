import React, { useEffect } from "react";
import "./App.scss";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import history from "./History";
import AdminLogin from "./components/admin/AdminLogin";
import Home from "./components/Home";
import Admin from "./components/admin/Admin";
import ServerResponse from "./ServerResponse";
import { useSelector, useDispatch } from "react-redux";
import { getFitnessSchedule, getCrossfitSchedule } from "./redux/workoutSchedule/scheduleActions";
import { handleSaveUser, updateWorkoutType } from "./redux/user/userActions";
import { handleAdminLogged } from "./redux/admin/adminActions";

function App() {
  const adminIsLogged = useSelector((state) => state.admin.adminIsLogged);
  const serverResponse = useSelector((state) => state.serverResponse.serverResponse);
  const userInfo = useSelector((state) => state.user.userInfo);
  const selectedSchedule = useSelector((state) => state.workoutSchedule.selectedSchedule);

  const accessToken = window.localStorage.accessToken;
  const adminRefreshToken = window.localStorage.adminRefreshToken;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFitnessSchedule());
    dispatch(getCrossfitSchedule());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (selectedSchedule.workoutType === "CROSSFIT") {
      if (serverResponse && serverResponse.message === "Trening je zakazan, vidimo se!" && !userInfo.crossfitActive) {
        dispatch(updateWorkoutType("CROSSFIT"));
      } else if (serverResponse && serverResponse.error === "Korisnik nije verifikovan za odabran program" && userInfo.crossfitActive) {
        dispatch(updateWorkoutType("CROSSFIT"));
      }
    }
    if (selectedSchedule.workoutType === "FITNESS") {
      if (serverResponse && serverResponse.message === "Trening je zakazan, vidimo se!" && !userInfo.fitnessActive) {
        dispatch(updateWorkoutType("FITNESS"));
      } else if (serverResponse && serverResponse.error === "Korisnik nije verifikovan za odabran program" && userInfo.fitnessActive) {
        dispatch(updateWorkoutType("FITNESS"));
      }
    }
    // eslint-disable-next-line
  }, [serverResponse, userInfo]);

  useEffect(() => {
    if (accessToken) dispatch(handleSaveUser(accessToken));
    if (adminRefreshToken) dispatch(handleAdminLogged());
    // eslint-disable-next-line
  }, []);

  return (
    <div className="App">
      <Router history={history}>
        <ServerResponse />
        <Switch>
          <Route exact path="/" render={() => <Home />} />
          <Route exact path="/admin" render={() => (adminIsLogged ? <Admin /> : <Redirect to="/login" />)} />
          <Route path="/login" render={() => (!adminIsLogged ? <AdminLogin /> : <Redirect to="/admin" />)} />
          <Route path="*" render={() => <div className="not-found">Status 404: Not Found. Wrong route path.</div>} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
