import React from "react";
import { useSelector } from "react-redux";

export default function ServerResponse() {
  const serverResponseMessage = useSelector((state) => state.serverResponse.serverResponse);
  const showServerResponse = useSelector((state) => state.serverResponse.showServerResponse);

  return (
    <div className={`server-response ${showServerResponse ? "" : "hidden"}`}>
      {serverResponseMessage && serverResponseMessage.error && (
        <div className="server-response-error">
          <h4>{serverResponseMessage.error}</h4>
        </div>
      )}
      {serverResponseMessage && serverResponseMessage.message && (
        <div className="server-response-message">
          <h4>{serverResponseMessage.message}</h4>
        </div>
      )}
    </div>
  );
}
