import React, { useState } from "react";
import { Element } from "react-scroll";
import Lightbox from "./Lightbox";
import { useSelector, useDispatch } from "react-redux";
import { handleShowImages } from "../redux/interface/interfaceActions";
import { crossfitImages, gymImages, fitnessImages } from "./GalleryImages";

function PhotoGallery() {
  const showImages = useSelector((state) => state.interface.showImages);
  const [displayedImages, setDisplayedImages] = useState("");

  const dispatch = useDispatch();

  const handleLightbox = (images) => {
    setDisplayedImages(images);
    dispatch(handleShowImages());
  };

  return (
    <Element className="gallery" name="galerija">
      <h1>GALERIJA</h1>
      <div className="gallery-body">
        <div className="gallery-folder">
          <img src={require("../images/crossfit-album.jpg")} alt="crossfit" onClick={() => handleLightbox(crossfitImages)} />
          <span>CROSSFIT</span>
        </div>
        <div className="gallery-folder">
          <img src={require("../images/gym.jpg")} alt="teretana" onClick={() => handleLightbox(gymImages)} />
          <span>TERETANA</span>
        </div>
        <div className="gallery-folder">
          <img src={require("../images/pump.jpg")} alt="fitness" onClick={() => handleLightbox(fitnessImages)} />
          <span>FITNESS</span>
        </div>
      </div>
      {showImages && (
        <div>
          <Lightbox handleLightbox={handleLightbox} displayedImages={displayedImages} />
        </div>
      )}
    </Element>
  );
}

export default PhotoGallery;
