import React from "react";
import { Element } from "react-scroll";
import { IoIosCloseCircleOutline } from "react-icons/io";

function ProgramInfo({ programInfo, handleProgramInfo, handleSignForWorkout }) {
  return (
    <Element className="programs" name="programi">
      <h1>{programInfo}</h1>
      <IoIosCloseCircleOutline className="close-icon" onClick={() => handleProgramInfo()} />
      {programInfo === "CROSSFIT" ? (
        <div className="program-info-body">
          <img src={require("../images/crossfit.jpg")} alt="crossfit" />
          <div>
            Krosfit predstavlja izuzetno intenzivno vežbanje koje se sastoji od nekoliko različitih treninga. Funkcionalni pokreti se izvode
            pod velikim intenzitetom čiji je cilj da naviknete telo na različite vežbe koje se nadovezuju jedna na drugu sa kratkim pauzama
            između njih.
            <br />
            <br /> Zbog toga će vas krosfit ojačati, učiniti fit u kratkom vremenskom intervalu, toniranim i mišićavim pomoću različitih
            treninga. Ti treninzi su kombinacija kardio vežbi, treninga snage, treninga sa opterećenjima, treninga izdržljivosti,
            gimnastičkih vežbi, često i plivanja i biciklizma.
            <br />
            <br />
            Oprobaj se i saznaj zašto je Krosfit postao jedan od najpopularnijih sportova kada je u pitanju fizička sprema.
            <br />
            <br />
            <ul>
              <li> MESEČNA ČLANARINA (4 PUTA NEDELJNO): 3000din</li>
              <li> DNEVNI TRENING (DROP IN): 500din</li>
              <li> PROBNI TRENING GRATIS</li>
            </ul>
          </div>
        </div>
      ) : programInfo === "TERETANA" ? (
        <div className="program-info-body">
          <img src={require("../images/gym.jpg")} alt="gym" />
          <div>
            Glavni cilj nam je uvek bio da pružimo najbolju i najkvalitetniju uslugu za naše vežbače i da konstantno tu uslugu unapređujemo.
            <br />
            <br />
            Predstavljamo vam novu <span>GVOŽĐAR TERETANU.</span> <br />
            <br />
            Teretana se prostire na 190m2 i opremljena je najsavremenijom fitness opremom. Idealna je za profesionalne sportiste kao i za
            rekreativce. Poseduje mnoštvo sprava za svaku grupu mišića i omogućava vrhunski trening. <br />
            <br />
            Radno vreme teretane je od 07:00 do 23:00.
            <br /> Čekamo vas!
            <br />
            <br />
            <ul>
              <li> DNEVNI TRENING (DROP IN): 500din</li>
              <li> MESEČNA ČLANARINA: 2800din</li>
              <li> PERSONALNI TRENING: 1200din</li>
              <li> MESEČNI PERSONALNI TRENINZI (16 TERMINA): 12000din</li>
              <li> PROBNI TRENING GRATIS</li>
            </ul>
          </div>
        </div>
      ) : programInfo === "FITNESS" ? (
        <div className="program-info-body">
          <img src={require("../images/fitness.jpg")} alt="pump" />
          <div>
            Sit cillum veniam elit exercitation cillum laborum sint ea irure mollit magna. Ex fugiat quis elit Lorem voluptate sunt sint
            reprehenderit enim consectetur consectetur nostrud nisi. Consectetur ut nostrud et et sunt occaecat enim nulla quis ad enim
            culpa non laboris. Id occaecat commodo sunt quis officia adipisicing. Minim ad enim ut ex officia cupidatat nulla aliquip nulla
            quis aute id. Sit anim proident laboris incididunt pariatur sunt laborum in est culpa quis esse. Sunt officia sit magna magna.{" "}
            <br />
            <br />
            <ul>
              <li> MESEČNA ČLANARINA (4 PUTA NEDELJNO): 3000din</li>
              <li> DNEVNI TRENING (DROP IN): 500din</li>
              <li> PROBNI TRENING GRATIS</li>
            </ul>
          </div>
        </div>
      ) : programInfo === "KOMBO" ? (
        <div className="program-info-body">
          <img src={require("../images/kombo.jpg")} alt="kombo" />
          <div>
            U Gvožđaru vam nudimo specijalne ponude za vežbače koji žele da se oprobaju u svim programima koje imamo. Iskoristi kombo ponudu
            i uštedi na članarini.
            <br />
            <br />
            Izaberi ponudu koja ti najviše odgovara i pridruži nam se već na sledećem treningu.
            <br />
            Vidimo se!
            <br />
            <br />
            <ul>
              <li> TERETANA + CROSSFIT (4 PUTA NEDELJNO): 4500din</li>
              <li> FITPASS ZA SVE PROGRAME VEŽBANJA</li>
              <li> 3 MESECA TERETANA + CROSSFIT (4 PUTA NEDELJNO): 12000</li>
            </ul>
          </div>
        </div>
      ) : (
        ""
      )}
      <button className="sign-for-workout-button" onClick={() => handleSignForWorkout()}>
        ZAKAŽI TRENING
      </button>
    </Element>
  );
}

export default ProgramInfo;
