import React, { useState } from "react";
import { IoIosArrowBack } from "react-icons/io";
import { IoIosArrowForward } from "react-icons/io";
import { IoIosCloseCircleOutline } from "react-icons/io";

function Lightbox({ handleLightbox, displayedImages }) {
  const [selectedPicture, setSelectedPicture] = useState(displayedImages[0]);

  const handleNextPicture = () => {
    if (selectedPicture.picNumber <= displayedImages.length - 1)
      setSelectedPicture(
        displayedImages.filter((img, index) => {
          return index === selectedPicture.picNumber;
        })[0]
      );
  };

  const handlePrevPicture = () => {
    if (selectedPicture.picNumber > 1)
      setSelectedPicture(
        displayedImages.filter((img, index) => {
          return index === selectedPicture.picNumber - 2;
        })[0]
      );
  };

  return (
    <div className="lightbox">
      <div className="lightbox-carousel">
        <IoIosCloseCircleOutline className="close-lightbox" onClick={() => handleLightbox()} />
        <IoIosArrowBack onClick={() => handlePrevPicture()} />
        <img src={selectedPicture.src} alt={selectedPicture.title} />
        <IoIosArrowForward onClick={() => handleNextPicture()} />
      </div>

      <p>
        {selectedPicture.picNumber} / {displayedImages.length}
      </p>
    </div>
  );
}

export default Lightbox;
