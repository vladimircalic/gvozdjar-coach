import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { handleGetUsers, handleVerifyUser } from "../../redux/admin/adminActions";
import { ImInfo } from "react-icons/im";
import { handleUserDetails } from "../../redux/admin/adminActions";

function Users() {
  const users = useSelector((state) => state.admin.users);
  const loading = useSelector((state) => state.admin.loading);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(handleGetUsers());
  }, [dispatch]);

  return (
    <div className="users">
      <h1>Svi korisnici</h1>
      <div className="users-table">
        <div className="table-header">
          <span>Ime</span>
          <span>Prezime</span>
          <span>Email</span>
          <span>Verifikovan</span>
          <span>Info</span>
        </div>
        <div className="table-body">
          <div className={`all-users ${loading ? "loading" : ""}`}>
            {users.map((user) => (
              <div className="user" key={user._id}>
                <span>{user.firstName}</span>
                <span>{user.lastName}</span>
                <span>{user.email}</span>
                <span className="verified">
                  <p
                    className={`${user.crossfitActive ? "is-active" : "not-active"}`}
                    onClick={() => dispatch(handleVerifyUser(user._id, "CROSSFIT"))}
                  >
                    Crossfit
                  </p>
                  <p
                    className={`${user.fitnessActive ? "is-active" : "not-active"}`}
                    onClick={() => dispatch(handleVerifyUser(user._id, "FITNESS"))}
                  >
                    Fitness
                  </p>
                </span>
                <span>
                  <ImInfo onClick={() => dispatch(handleUserDetails(user))} />
                </span>
              </div>
            ))}
          </div>
        </div>
        <div className={`loader ${!loading ? "none" : ""}`}></div>
      </div>
    </div>
  );
}

export default Users;
