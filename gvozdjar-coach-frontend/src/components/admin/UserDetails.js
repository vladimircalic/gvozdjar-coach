import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { handleShowUserDetails } from "../../redux/interface/interfaceActions";
import { handleDeleteUser } from "../../redux/admin/adminActions";

function UserDetails() {
  const showUserDetails = useSelector((state) => state.interface.showUserDetails);
  const selectedUser = useSelector((state) => state.admin.selectedUser);

  const dispatch = useDispatch();

  return (
    <div className={`user-details ${showUserDetails ? "" : "hidden"}`}>
      <div className="user-details-header">
        <IoIosCloseCircleOutline className="close-icon" onClick={() => dispatch(handleShowUserDetails())} />
        <h1>
          {selectedUser.firstName} {selectedUser.lastName}
        </h1>
      </div>
      <div className="user-details-body">
        <p>User info</p>
      </div>
      <button className="form-button" onClick={() => dispatch(handleDeleteUser(selectedUser._id))}>
        Obriši korisnika
      </button>
    </div>
  );
}

export default UserDetails;
