import React from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { handleAdminLogin } from "../../redux/admin/adminActions";

export default function AdminLogin() {
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();

  const onSubmit = (data) => {
    dispatch(handleAdminLogin(data));
  };

  return (
    <div className="login-page">
      <form className="login-page-form" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-header">
          <h1>Login</h1>
        </div>
        <div className="form-body">
          <input placeholder="Username" type="text" name="userName" ref={register} />
          <input placeholder="Password" type="password" name="password" ref={register} />
          <button type="submit" className="btn btn-primary">
            Login
          </button>
        </div>
      </form>
    </div>
  );
}
