import React from "react";
import WorkoutSchedule from "../WorkoutSchedule";
import Users from "./Users";
import UserDetails from "./UserDetails";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { AiOutlineDelete } from "react-icons/ai";
import { useSelector, useDispatch } from "react-redux";
import { handleShowWorkoutDetails } from "../../redux/interface/interfaceActions";
import { handleAdminLogout, handleRemoveAttendant } from "../../redux/admin/adminActions";

function Admin() {
  const showWorkoutDetails = useSelector((state) => state.interface.showWorkoutDetails);
  const workoutType = useSelector((state) => state.workoutSchedule.selectedSchedule.workoutType);
  const admin = useSelector((state) => state.admin.adminIsLogged);
  const selectedWorkout = useSelector((state) => state.admin.selectedWorkout);
  const selectedWorkoutAttendants = useSelector((state) => state.admin.selectedWorkoutAttendants);
  const loading = useSelector((state) => state.admin.loading);

  const dispatch = useDispatch();

  return (
    <div className="home-page">
      <div className="home-page-header">
        <h1>ADMIN</h1>
      </div>
      <div className="home-page-body">
        <WorkoutSchedule admin={admin} />
        <div className={`workout-details ${showWorkoutDetails ? "" : "hidden"}`}>
          <div className={`loader ${!loading ? "none" : ""}`}></div>
          <div className="workout-details-header">
            <h1>
              {selectedWorkout.day.day}, {selectedWorkout.day.date} <br /> {selectedWorkout.class.time}
              <IoIosCloseCircleOutline className="close-icon" onClick={() => dispatch(handleShowWorkoutDetails())} />
            </h1>
          </div>
          <div className={`workout-details-body ${loading ? "loading" : ""}`}>
            {selectedWorkoutAttendants.map((att) => (
              <div key={att._id} className="attendant">
                {att.userName} <AiOutlineDelete onClick={() => dispatch(handleRemoveAttendant(att._id, workoutType))} />
              </div>
            ))}
          </div>
        </div>
        <Users />
        <UserDetails />
        <button onClick={() => dispatch(handleAdminLogout())} className="logout-button">
          Logout
        </button>
      </div>
    </div>
  );
}

export default Admin;
