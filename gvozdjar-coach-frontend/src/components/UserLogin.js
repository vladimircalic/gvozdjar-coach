import React from "react";
import { useDispatch } from "react-redux";
import { showRegisterForm } from "../redux/interface/interfaceActions";
import { useForm } from "react-hook-form";
import { handleUserLogin } from "../redux/user/userActions";

function UserLogin() {
  const { register, handleSubmit, errors } = useForm();

  const dispatch = useDispatch();

  const onSubmit = (data) => {
    dispatch(handleUserLogin(data));
  };

  return (
    <form className="user-login" onSubmit={handleSubmit(onSubmit)}>
      <div className="user-login-header">
        <h2>PRIJAVA KORISNIKA</h2>
      </div>
      <div className="user-login-body">
        <input
          placeholder="Email adresa"
          name="email"
          ref={register({
            required: true,
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
              message: "Enter a valid e-mail address",
            },
          })}
        />
        <input placeholder="Lozinka" type="password" name="password" ref={register({ required: true })} />
        {errors.password ? (
          <div className="error-message">Sva polja moraju biti popunjena</div>
        ) : (
          errors.email && <div className="error-message">{errors.email.message}</div>
        )}
      </div>
      <button type="submit" className="form-button">
        Prijavi se
      </button>
      <div>
        <span>
          Nemaš nalog?{" "}
          <span className="change-info" onClick={() => dispatch(showRegisterForm())}>
            Registruj se !
          </span>
        </span>
      </div>
    </form>
  );
}

export default UserLogin;
