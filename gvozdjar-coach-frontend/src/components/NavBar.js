import React from "react";
import gvozdjarLogo from "../images/gvozdjar-grb.jpg";
import { Link } from "react-scroll";
import { GiHamburgerMenu } from "react-icons/gi";
import { AiOutlineUser } from "react-icons/ai";
import { useSelector, useDispatch } from "react-redux";
import { handleShowUserInfo, handleShowSidebar } from "../redux/interface/interfaceActions";

function NavBar() {
  const showSidebar = useSelector((state) => state.interface.showSidebar);
  const userIsLogged = useSelector((state) => state.user.userIsLogged);

  const dispatch = useDispatch();

  return (
    <div className="home-page-nav">
      <GiHamburgerMenu onClick={() => dispatch(handleShowSidebar())} className="hamburger-menu" />
      <img src={gvozdjarLogo} alt="logo" />
      <div className="nav-main">
        <h1>TRENING CENTAR GVOŽĐAR</h1>
        <div className={`nav-routes ${showSidebar ? "" : "hidden"}`}>
          <div className="spreader">|</div>
          <span>
            <Link
              activeClass="active"
              to="pocetna"
              spy={true}
              smooth={true}
              duration={800}
              offset={-150}
              onClick={() => dispatch(handleShowSidebar())}
            >
              Početna
            </Link>
          </span>
          <div className="spreader">|</div>
          <span>
            <Link
              activeClass="active"
              to="programi"
              spy={true}
              smooth={true}
              duration={800}
              offset={-150}
              onClick={() => dispatch(handleShowSidebar())}
            >
              Programi
            </Link>
          </span>
          <div className="spreader">|</div>
          <span>
            <Link
              activeClass="active"
              to="raspored"
              spy={true}
              smooth={true}
              duration={800}
              offset={-300}
              onClick={() => dispatch(handleShowSidebar())}
            >
              Raspored
            </Link>
          </span>
          <div className="spreader">|</div>
          <span>
            <Link
              activeClass="active"
              to="o-nama"
              spy={true}
              smooth={true}
              duration={800}
              offset={-180}
              onClick={() => dispatch(handleShowSidebar())}
            >
              O nama
            </Link>
          </span>
          <div className="spreader">|</div>
          <span>
            <Link
              activeClass="active"
              to="galerija"
              spy={true}
              smooth={true}
              duration={800}
              offset={-150}
              onClick={() => dispatch(handleShowSidebar())}
            >
              Galerija
            </Link>
          </span>
          <div className="spreader">|</div>
          <span>
            <Link
              activeClass="active"
              to="kontakt"
              spy={true}
              smooth={true}
              duration={800}
              offset={-180}
              onClick={() => dispatch(handleShowSidebar())}
            >
              Kontakt
            </Link>
          </span>
          <div className="spreader">|</div>
        </div>
      </div>
      <AiOutlineUser className={`user-icon ${userIsLogged ? "online" : ""}`} onClick={() => dispatch(handleShowUserInfo())} />
    </div>
  );
}

export default NavBar;
