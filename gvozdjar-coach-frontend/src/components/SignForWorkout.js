import React, { useState, useRef } from "react";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { useSelector, useDispatch } from "react-redux";
import { submitSignForWorkout } from "../redux/user/userActions";

function SignForWorkout({ shortSignForWorkout, handleSignForWorkout }) {
  const crossfitSchedule = useSelector((state) => state.workoutSchedule.crossfitSchedule);
  const fitnessSchedule = useSelector((state) => state.workoutSchedule.fitnessSchedule);
  const showSignForWorkout = useSelector((state) => state.interface.showSignForWorkout);
  const user = useSelector((state) => state.user.userInfo);

  const [selectedDay, setSelectedDay] = useState("");
  const [errorMessage, setErrorMessage] = useState(false);

  const workoutType = useRef();
  const trainingDay = useRef();
  const trainingClass = useRef();
  const dispatch = useDispatch();

  const handleResetForm = () => {
    workoutType.current.value = "";
    trainingDay.current.value = "";
    trainingClass.current.value = "";

    setSelectedDay("");
    setErrorMessage(false);
  };

  const closeSignForWorkout = () => {
    handleResetForm();
    handleSignForWorkout();
  };

  const handleSignForWorkoutForm = () => {
    const selectedClass =
      !shortSignForWorkout &&
      selectedDay &&
      selectedDay.classes.filter((cl) => {
        if (trainingClass.current.value.includes(cl.time)) return cl.time;
        else return null;
      })[0];

    const trainingRegistration = {
      userId: user.userId,
      userName: user.firstName + " " + user.lastName,
      workoutType: workoutType.current.value,
      trainingDate: shortSignForWorkout ? shortSignForWorkout.day.date : selectedDay && selectedDay.date,
      trainingDay: shortSignForWorkout ? shortSignForWorkout.day.day : selectedDay && selectedDay.day,
      trainingClass: shortSignForWorkout ? shortSignForWorkout.class.time : selectedClass && selectedClass.time,
    };
    if (
      !trainingRegistration.workoutType ||
      !trainingRegistration.trainingDate ||
      !trainingRegistration.trainingDay ||
      !trainingRegistration.trainingClass
    ) {
      setErrorMessage(true);
    } else {
      setErrorMessage(false);
      dispatch(submitSignForWorkout(trainingRegistration));
      closeSignForWorkout();
    }
  };

  const handleSchedule = () => {
    if (workoutType.current.value === "CROSSFIT")
      setSelectedDay(
        crossfitSchedule.schedule.filter((day) => {
          if (trainingDay.current.value.includes(day.day)) return day;
          else return null;
        })[0]
      );

    if (workoutType.current.value === "FITNESS")
      setSelectedDay(
        fitnessSchedule.schedule.filter((day) => {
          if (trainingDay.current.value.includes(day.day)) return day;
          else return null;
        })[0]
      );
  };

  return (
    <div className={`sign-for-workout ${showSignForWorkout ? "" : "hidden"}`}>
      <div className="sign-for-workout-header">
        <IoIosCloseCircleOutline className="close-icon" onClick={() => closeSignForWorkout()} />
        <img src={require("../images/gvozdjar-grb.jpg")} alt="grb" />
        <h2>ZAKAŽI TRENING</h2>
      </div>
      <div className={`sign-for-workout-body ${shortSignForWorkout ? "filled" : ""}`}>
        <input placeholder={`${user.firstName} ${user.lastName}`} disabled />
        <select ref={workoutType} onChange={() => handleSchedule()}>
          {shortSignForWorkout ? (
            <option>{shortSignForWorkout.workoutType}</option>
          ) : (
            <>
              <option value="" hidden>
                Izaberi program treninga
              </option>
              <option>CROSSFIT</option>
              <option>FITNESS</option>
            </>
          )}
        </select>
        <select ref={trainingDay} onChange={() => handleSchedule()}>
          {shortSignForWorkout ? (
            <option>
              {shortSignForWorkout.day.day}, {shortSignForWorkout.day.date}
            </option>
          ) : (
            <>
              <option value="" hidden>
                Izaberi dan
              </option>
              {crossfitSchedule.schedule &&
                crossfitSchedule.schedule.map((day, index) => (
                  <option key={index}>
                    {day.day}, {day.date}
                  </option>
                ))}
            </>
          )}
        </select>
        <select ref={trainingClass}>
          {shortSignForWorkout ? (
            <option>
              {shortSignForWorkout.class.time} {shortSignForWorkout.class.classType}
            </option>
          ) : (
            <>
              <option value="" hidden>
                Izaberi termin
              </option>
              {!selectedDay && <option disabled>Izaberite program i dan treninga </option>}
              {selectedDay &&
                selectedDay.classes.map((c, index) => (
                  <option key={index}>
                    {c.time} {c.classType}
                  </option>
                ))}
            </>
          )}
        </select>
        {errorMessage && <div className="error-message">Sva polja moraju biti popunjena</div>}
      </div>
      <button className="form-button" onClick={() => handleSignForWorkoutForm()}>
        Zakaži
      </button>
    </div>
  );
}

export default SignForWorkout;
