import React from "react";
import UserLogin from "./UserLogin";
import UserRegister from "./UserRegister";
import { useSelector, useDispatch } from "react-redux";
import { AiOutlineUser } from "react-icons/ai";
import { IoIosCloseCircleOutline } from "react-icons/io";
import { handleUserLogout, handleGetUserWorkouts, handleClearUserWorkouts, cancelWorkoutRegistration } from "../redux/user/userActions";
import { closeUserInfo } from "../redux/interface/interfaceActions";
import { AiOutlineDelete } from "react-icons/ai";

function UserInfo() {
  const userIsLogged = useSelector((state) => state.user.userIsLogged);
  const showUserInfo = useSelector((state) => state.interface.showUserInfo);
  const showRegisterForm = useSelector((state) => state.interface.showRegisterForm);
  const showLoginForm = useSelector((state) => state.interface.showLoginForm);
  const user = useSelector((state) => state.user.userInfo);
  const showUserWorkouts = useSelector((state) => state.interface.showUserWorkouts);
  const userWorkouts = useSelector((state) => state.user.userWorkouts);
  const loading = useSelector((state) => state.user.loading);

  const dispatch = useDispatch();

  return (
    <div className={`user-info ${showUserInfo ? "" : "hidden"}`}>
      <div className="user-info-header">
        <AiOutlineUser className="info-svg" />
        <IoIosCloseCircleOutline className="close-icon" onClick={() => dispatch(closeUserInfo())} />
      </div>

      {userIsLogged ? (
        <div className="info-details">
          <div className="info-header">
            <h2>DETALJI O KORISNIKU</h2>
          </div>
          <div className="info-body">
            <div className="info-options">
              <span className={`${!showUserWorkouts ? "active" : ""}`} onClick={() => dispatch(handleClearUserWorkouts())}>
                Detalji
              </span>{" "}
              |{" "}
              <span className={`${showUserWorkouts ? "active" : ""}`} onClick={() => dispatch(handleGetUserWorkouts())}>
                Treninzi
              </span>
            </div>
            <p>
              Ime: <span>{user.firstName} </span>
            </p>
            <p>
              Prezime: <span>{user.lastName} </span>
            </p>
            <p>
              Broj telefona: <span>{user.phoneNumber} </span>
            </p>
            <p>
              CROSSFIT:{" "}
              <span className={`${user.crossfitActive ? "is-active" : "not-active"}`}>
                {user.crossfitActive ? "Aktivan" : "Nije aktivan"}
              </span>
            </p>
            <p>
              FITNESS:{" "}
              <span className={`${user.fitnessActive ? "is-active" : "not-active"}`}>
                {user.fitnessActive ? "Aktivan" : "Nije aktivan"}
              </span>
            </p>
            <div className={`user-workouts ${showUserWorkouts ? "" : "hidden"}`}>
              <div className={`all-workouts ${loading ? "loading" : ""}`}>
                {userWorkouts.map((workout, index) => (
                  <div className="workout" key={index}>
                    <span>{workout.workoutType}</span>
                    <span>{workout.trainingDay.substring(0, 3)}</span>
                    <span>{workout.trainingDate}</span>
                    <span>{workout.trainingClass}</span>
                    <AiOutlineDelete onClick={() => dispatch(cancelWorkoutRegistration(workout))} />
                  </div>
                ))}
              </div>
              <div className={`loader ${!loading ? "none" : ""}`}></div>
            </div>
          </div>
          <button className="form-button" onClick={() => dispatch(handleUserLogout())}>
            Logout
          </button>
        </div>
      ) : showLoginForm ? (
        <UserLogin />
      ) : showRegisterForm ? (
        <UserRegister />
      ) : (
        ""
      )}
    </div>
  );
}

export default UserInfo;
