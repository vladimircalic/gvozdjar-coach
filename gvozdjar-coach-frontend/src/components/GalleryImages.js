export const crossfitImages = [
  {
    src: require("../images/crossfitGallery/crossfit-album.jpg"),
    title: "Crossfit pic",
    picNumber: 1,
  },
  {
    src: require("../images/crossfitGallery/crossfit-info.jpg"),
    title: "Crossfit pic",
    picNumber: 2,
  },
  {
    src: require("../images/crossfitGallery/crossfit.jpg"),
    title: "Crossfit pic",
    picNumber: 3,
  },
];

export const gymImages = [
  {
    src: require("../images/gymGallery/gym-picture-1.JPG"),
    title: "Gym pic",
    picNumber: 1,
  },
  {
    src: require("../images/gymGallery/gym-picture-2.JPG"),
    title: "Gym pic",
    picNumber: 2,
  },
  {
    src: require("../images/gymGallery/gym-picture-3.JPG"),
    title: "Gym pic",
    picNumber: 3,
  },
];

export const fitnessImages = [
  {
    src: require("../images/fitnessGallery/fitness-picture-1.jpg"),
    title: "Gym pic",
    picNumber: 1,
  },
  {
    src: require("../images/fitnessGallery/fitness-picture-2.jpg"),
    title: "Gym pic",
    picNumber: 2,
  },
  {
    src: require("../images/fitnessGallery/fitness-picture-3.jpg"),
    title: "Gym pic",
    picNumber: 3,
  },
  {
    src: require("../images/fitnessGallery/fitness-picture-4.jpg"),
    title: "Gym pic",
    picNumber: 4,
  },
];
