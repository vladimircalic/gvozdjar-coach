import React from "react";
import { useDispatch } from "react-redux";
import { showLoginForm } from "../redux/interface/interfaceActions";
import { useForm } from "react-hook-form";
import { handleUserRegister } from "../redux/user/userActions";

function UserRegister() {
  const { register, handleSubmit, errors } = useForm();

  const dispatch = useDispatch();

  const onSubmit = (data) => {
    dispatch(handleUserRegister(data));
  };

  return (
    <form className="user-register" onSubmit={handleSubmit(onSubmit)}>
      <div className="user-register-header">
        <h2>REGISTRACIJA KORISNIKA</h2>
      </div>
      <div className="user-register-body">
        <input
          placeholder="Email adresa"
          name="email"
          ref={register({
            required: true,
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
              message: "Enter a valid e-mail address",
            },
          })}
        />
        <input placeholder="Ime" name="firstName" ref={register({ required: true })} />
        <input placeholder="Prezime" name="lastName" ref={register({ required: true })} />
        <input placeholder="Broj telefona" name="phoneNumber" ref={register({ required: true })} />
        <input placeholder="Lozinka" type="password" name="password" ref={register({ required: true })} />
        {errors.firstName || errors.lastName || errors.password ? (
          <div className="error-message">Sva polja moraju biti popunjena</div>
        ) : (
          errors.email && <div className="error-message">{errors.email.message}</div>
        )}
      </div>
      <button className="form-button" type="submit">
        Registruj se
      </button>
      <div>
        <span>
          Već imaš nalog?{" "}
          <span className="change-info" onClick={() => dispatch(showLoginForm())}>
            Prijavi se !
          </span>
        </span>
      </div>
    </form>
  );
}

export default UserRegister;
