import React from "react";
import { useSelector } from "react-redux";

function Backdrop() {
  const showBackdrop = useSelector((state) => state.interface.showBackdrop);

  if (showBackdrop) document.body.style.overflow = "hidden";
  else document.body.style.overflow = "scroll";

  return <div className={`backdrop ${showBackdrop ? "" : "hidden"}`}></div>;
}

export default Backdrop;
