import React from "react";
import { Element } from "react-scroll";

function Programs({ handleProgramInfo }) {
  return (
    <Element className="programs" name="programi">
      <h1>PROGRAMI</h1>
      <div className="programs-body">
        <div className="program" onClick={() => handleProgramInfo("CROSSFIT")}>
          <span>CROSSFIT</span>
          <img src={require("../images/crossfit.jpg")} alt="crossfit" />
        </div>
        <div className="program" onClick={() => handleProgramInfo("TERETANA")}>
          <span>TERETANA</span>
          <img src={require("../images/gym.jpg")} alt="gym" />
        </div>
        <div className="program" onClick={() => handleProgramInfo("FITNESS")}>
          <span>FITNESS</span>
          <img src={require("../images/fitness.jpg")} alt="pump" />
        </div>
        <div className="program" onClick={() => handleProgramInfo("KOMBO")}>
          <span>KOMBO</span>
          <img src={require("../images/kombo.jpg")} alt="kombo" />
        </div>
      </div>
    </Element>
  );
}

export default Programs;
