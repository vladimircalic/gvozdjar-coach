import React, { useState } from "react";
import NavBar from "./NavBar";
import UserInfo from "./UserInfo";
import Cover from "./Cover";
import WorkoutSchedule from "./WorkoutSchedule";
import Programs from "./Programs";
import AboutUs from "./AboutUs";
import PhotoGallery from "./PhotoGallery";
import Contact from "./Contact";
import Footer from "./Footer";
import SignForWorkout from "./SignForWorkout";
import ProgramInfo from "./ProgramInfo";
import Backdrop from "./Backdrop";
import { useDispatch, useSelector } from "react-redux";
import { handleShowSignForWorkout, handleShowUserInfo } from "../redux/interface/interfaceActions";
// eslint-disable-next-line
import { animateScroll as scrollSpy, scroller } from "react-scroll";

export default function Home() {
  const [programInfo, setProgramInfo] = useState("");
  const [shortSignForWorkout, setShortSignForWorkout] = useState("");

  const userIsLogged = useSelector((state) => state.user.userIsLogged);

  const dispatch = useDispatch();

  const handleSignForWorkout = (type, day, c) => {
    if (userIsLogged) {
      dispatch(handleShowSignForWorkout());
      if (type && day && c) {
        setShortSignForWorkout({ workoutType: type, day: day, class: c });
      } else {
        setShortSignForWorkout("");
      }
    } else dispatch(handleShowUserInfo());
  };
  const handleProgramInfo = (info) => {
    if (programInfo === "") {
      setProgramInfo(info);
    } else {
      setProgramInfo("");
    }
    scroller.scrollTo("programi", {
      duration: 800,
      smooth: true,
      offset: -150,
    });
  };

  return (
    <div className="home-page">
      <NavBar />

      <div className="home-page-body">
        <Cover handleSignForWorkout={handleSignForWorkout} />
        <UserInfo />
        <SignForWorkout handleSignForWorkout={handleSignForWorkout} shortSignForWorkout={shortSignForWorkout} />
        {!programInfo ? (
          <Programs handleProgramInfo={handleProgramInfo} />
        ) : (
          <ProgramInfo programInfo={programInfo} handleProgramInfo={handleProgramInfo} handleSignForWorkout={handleSignForWorkout} />
        )}
        <WorkoutSchedule handleSignForWorkout={handleSignForWorkout} />
        <AboutUs />
        <PhotoGallery />
        <Contact />
        <Backdrop />
      </div>
      <Footer />
    </div>
  );
}
