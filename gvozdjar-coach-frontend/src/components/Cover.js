import React from "react";
import { Carousel } from "react-bootstrap";
import { Element } from "react-scroll";

function Cover({ handleSignForWorkout }) {
  return (
    <Element className="background-cover" name="pocetna">
      <Carousel>
        <Carousel.Item>
          <img className="d-block w-100" src={require("../images/carousel-1.jpg")} alt="First slide" />
          <Carousel.Caption>
            <h3>PRONADJI ZVER U SEBI</h3>
            <p>Treniraj sa najjačima.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={require("../images/carousel-2.jpg")} alt="Third slide" />

          <Carousel.Caption>
            <h3>DOVEDI SE U TOP FORMU </h3>
            <p>Dodji do željene forme uz proveren način treninga.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="d-block w-100" src={require("../images/carousel-3.jpg")} alt="Third slide" />

          <Carousel.Caption>
            <h3>POSTANI DEO EKIPE</h3>
            <p>Treniraj sa nama i uveri se zašto smo najbolji.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <button className="sign-for-workout-button" onClick={() => handleSignForWorkout()}>
        ZAKAŽI TRENING
      </button>
    </Element>
  );
}

export default Cover;
