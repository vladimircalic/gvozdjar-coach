import React from "react";

function Footer() {
  return (
    <div className="footer">
      {/* <h1>Footer</h1> */}
      <div className="footer-icons">
        <a href="https://www.instagram.com/treningcentargvozdjar/" target="blank">
          {/* <FaInstagram className="instagram" /> */}
          <img alt="instagram logo" src={require("../images/instagram-icon.png")} className="instagram" />
        </a>
        <a href="https://www.facebook.com/treningcentargvozdjar/" target="blank">
          <img alt="facebook logo" src={require("../images/facebook-icon.png")} className="facebook" />
          {/* <FaFacebookSquare className="facebook" /> */}
        </a>
      </div>
      <p>Copyright 2016 Crossbox Gvožđar.‌‌ All Rights Reserved.</p>
    </div>
  );
}

export default Footer;
