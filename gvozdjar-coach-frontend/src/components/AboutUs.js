import React from "react";
import { Element } from "react-scroll";

function AboutUs() {
  return (
    <Element className="about-us" name="o-nama">
      <h1>O NAMA</h1>
      <div className="about-us-body">
        <div className="about-us-info">
          Eu aute anim consequat duis anim reprehenderit dolor adipisicing sit ullamco deserunt fugiat. Excepteur eiusmod incididunt tempor
          non quis ex deserunt tempor do ipsum exercitation. Veniam esse veniam id ut pariatur tempor. Labore occaecat sit ad sit
          reprehenderit commodo dolor non duis ipsum elit occaecat.Elit labore consectetur consectetur nulla minim mollit amet magna esse.
          Amet ad culpa veniam consectetur elit ipsum magna Lorem non. Sunt veniam mollit quis duis reprehenderit enim. Non dolore laboris
          qui tempor laboris nisi minim culpa nulla consectetur amet voluptate. Consequat officia quis qui cillum excepteur fugiat est
          excepteur. Aliquip Lorem proident esse ex non minim eiusmod. Cillum laboris sit enim adipisicing labore aute duis duis nulla
          occaecat consequat culpa laborum fugiat.
          <br />
          <br />
          Qui tempor labore sunt ullamco eiusmod ipsum culpa exercitation mollit pariatur esse irure ut labore. Ad sunt sit tempor commodo
          duis cupidatat irure est esse dolor in. Ad amet pariatur ex qui cillum ea do consectetur in cillum consequat. Do eiusmod sunt
          aliqua duis officia. Eu commodo laborum deserunt exercitation reprehenderit ipsum ex id. Ullamco eiusmod laborum et veniam Lorem
          nulla proident nostrud exercitation amet et.
        </div>
        <img src={require("../images/carousel-3.jpg")} alt="about us" />
      </div>
    </Element>
  );
}

export default AboutUs;
