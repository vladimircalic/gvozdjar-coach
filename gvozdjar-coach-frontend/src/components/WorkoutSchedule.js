import React, { useEffect } from "react";
import { Element } from "react-scroll";
import { IoIosArrowBack } from "react-icons/io";
import { IoIosArrowForward } from "react-icons/io";
import { useSelector, useDispatch } from "react-redux";
import { getWorkoutSchedule } from "../redux/workoutSchedule/scheduleActions";
import { handleWorkoutInfo } from "../redux/admin/adminActions";

function WorkoutSchedule({ handleSignForWorkout, admin }) {
  const selectedSchedule = useSelector((state) => state.workoutSchedule.selectedSchedule);
  const loading = useSelector((state) => state.workoutSchedule.loading);

  require("moment/locale/sr");

  const dispatch = useDispatch();

  const toggleSelectedSchedule = () => {
    if (selectedSchedule.workoutType === "CROSSFIT") dispatch(getWorkoutSchedule("FITNESS"));
    if (selectedSchedule.workoutType === "FITNESS") dispatch(getWorkoutSchedule("CROSSFIT"));
  };

  useEffect(() => {
    dispatch(getWorkoutSchedule("CROSSFIT"));
  }, [dispatch]);

  return (
    <div className="workout-schedule">
      <h1>RASPORED TRENINGA</h1>
      <div className="selected-schedule">
        <IoIosArrowBack onClick={() => toggleSelectedSchedule()} />
        <h3>{selectedSchedule.workoutType} GRUPE</h3>
        <IoIosArrowForward onClick={() => toggleSelectedSchedule()} />
      </div>

      <Element className="logs-table" id="raspored">
        <div className="table-header">
          {selectedSchedule.schedule &&
            selectedSchedule.schedule.map((day, index) => (
              <span key={index}>
                {day.day} <p>{day.date}</p>
              </span>
            ))}
        </div>
        <div className="table-body">
          <div className={`week-schedule ${loading ? "loading" : ""}`}>
            {selectedSchedule.schedule &&
              selectedSchedule.schedule.map((day, index) => {
                return (
                  <div className="day-schedule" key={index}>
                    {day.classes.map((c, index) => {
                      return (
                        <p
                          key={index}
                          onClick={() =>
                            admin
                              ? dispatch(handleWorkoutInfo(selectedSchedule.workoutType, day, c))
                              : handleSignForWorkout(selectedSchedule.workoutType, day, c)
                          }
                        >
                          {c.time} {c.classType} <span className={`${c.attendants >= 12 ? "full" : ""}`}> {`(${c.attendants})`}</span>
                        </p>
                      );
                    })}
                  </div>
                );
              })}
          </div>
          <div className={`loader ${!loading ? "none" : ""}`}></div>
        </div>
      </Element>
    </div>
  );
}

export default WorkoutSchedule;
