import React from "react";
import { Element } from "react-scroll";
import { RiFacebookBoxLine } from "react-icons/ri";
import { FaInstagram } from "react-icons/fa";
import { AiOutlineMail } from "react-icons/ai";
import { AiOutlinePhone } from "react-icons/ai";
import { HiOutlineLocationMarker } from "react-icons/hi";

function Contact() {
  return (
    <Element className="contact" name="kontakt">
      <h1>KONTAKT</h1>
      <div className="contact-body">
        <div className="contact-links">
          <p>
            <HiOutlineLocationMarker />
            Vojvode Petra Bojovica 1
          </p>
          <p>
            <AiOutlinePhone />
            063/111-22-33
          </p>
          <p>
            <AiOutlineMail />
            blabla@gmail.com
          </p>
          <p>
            <FaInstagram />
            <a href="https://www.instagram.com/treningcentargvozdjar/" target="blank">
              instagram.com/treningcentargvozdjar
            </a>
          </p>
          <p>
            <RiFacebookBoxLine />
            <a href="https://www.facebook.com/treningcentargvozdjar/" target="blank">
              facebook.com/treningcentargvozdjar
            </a>
          </p>
        </div>

        <iframe
          title="Gvožđar"
          src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Vojvode%20Petra%20Bojovi%C4%87a%201,%20Pan%C4%8Devo+(Gvo%C5%BE%C4%91ar)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
        ></iframe>
      </div>
    </Element>
  );
}

export default Contact;
