import {
  HANDLE_USER_LOGGED_IN,
  SAVE_USER,
  SAVE_USER_WORKOUTS,
  HANDLE_LOADING,
  CLEAR_USER_WORKOUTS,
  UPDATE_FITNESS_ACTIVE,
  UPDATE_CROSSFIT_ACTIVE,
} from "./userTypes";

const initialState = {
  userIsLogged: false,
  userInfo: {},
  loading: false,
  userWorkouts: [],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_LOADING: {
      return {
        ...state,
        loading: !state.loading,
      };
    }
    case HANDLE_USER_LOGGED_IN:
      return {
        ...state,
        userIsLogged: !state.userIsLogged,
      };
    case SAVE_USER:
      return {
        ...state,
        userInfo: action.payload,
      };
    case SAVE_USER_WORKOUTS:
      return {
        ...state,
        userWorkouts: action.payload,
      };
    case CLEAR_USER_WORKOUTS:
      return {
        ...state,
        userWorkouts: [],
      };
    case UPDATE_FITNESS_ACTIVE:
      return {
        ...state,
        userInfo: { ...state.userInfo, fitnessActive: !state.userInfo.fitnessActive },
      };
    case UPDATE_CROSSFIT_ACTIVE:
      return {
        ...state,
        userInfo: { ...state.userInfo, crossfitActive: !state.userInfo.crossfitActive },
      };
    default:
      return state;
  }
};

export default userReducer;
