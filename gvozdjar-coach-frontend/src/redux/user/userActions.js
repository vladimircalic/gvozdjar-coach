import {
  HANDLE_USER_LOGGED_IN,
  SAVE_USER,
  SAVE_USER_WORKOUTS,
  HANDLE_LOADING,
  CLEAR_USER_WORKOUTS,
  UPDATE_CROSSFIT_ACTIVE,
  UPDATE_FITNESS_ACTIVE,
} from "./userTypes";
import axios from "axios";
import { handleServerResponse } from "../serverResponse/serverResponseActions";
import { closeUserInfo, showLoginForm, handleShowUserWorkouts, handleHideUserWorkouts } from "../interface/interfaceActions";
import { getWorkoutSchedule } from "../workoutSchedule/scheduleActions";

const axiosWithAuth = axios.create();
const jwt = require("jsonwebtoken");

export const handleLoading = () => {
  return {
    type: HANDLE_LOADING,
  };
};

export const handleUserIsLogged = () => {
  return {
    type: HANDLE_USER_LOGGED_IN,
  };
};

export const saveUser = (user) => {
  return {
    type: SAVE_USER,
    payload: user,
  };
};

export const handleSaveUser = (accessToken) => {
  return (dispatch) => {
    const user = jwt.decode(accessToken);
    dispatch(saveUser(user));
    dispatch(handleUserIsLogged());
  };
};

export const handleUserLogin = (data) => {
  return (dispatch) => {
    axios
      .post("/api/user-login", data)
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleSaveUser(res.data.accessToken));
        dispatch(closeUserInfo());
        window.localStorage.setItem("accessToken", res.data.accessToken);
        window.localStorage.setItem("refreshToken", res.data.refreshToken);
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
      });
  };
};

export const handleUserRegister = (data) => {
  return (dispatch) => {
    axios
      .post("/api/user-register", data)
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(showLoginForm());
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
      });
  };
};

export const handleUserLogout = () => {
  return (dispatch) => {
    axios
      .delete("/api/user-logout", {
        headers: { refreshToken: window.localStorage.refreshToken },
      })
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleUserIsLogged());
        dispatch(closeUserInfo());
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshToken");
      })
      .catch((err) => dispatch(handleServerResponse(err.response.data)));
  };
};

export const submitSignForWorkout = (trainingRegistration) => {
  return (dispatch) => {
    axiosWithAuth
      .post("/api/workout-registration", trainingRegistration)
      .then((res) => {
        dispatch(getWorkoutSchedule(trainingRegistration.workoutType));
        dispatch(handleServerResponse(res.data));
      })
      .catch((err) => {
        dispatch(getWorkoutSchedule(trainingRegistration.workoutType));
        dispatch(handleServerResponse(err.response.data));
      });
  };
};

export const saveUserWorkout = (userWorkouts) => {
  return {
    type: SAVE_USER_WORKOUTS,
    payload: userWorkouts,
  };
};

export const handleGetUserWorkouts = () => {
  return (dispatch) => {
    dispatch(handleShowUserWorkouts());
    dispatch(handleLoading());
    axiosWithAuth
      .get("/api/user-workouts")
      .then((res) => {
        dispatch(saveUserWorkout(res.data));
        dispatch(handleLoading());
      })
      .catch((err) => {
        console.log(err);
        dispatch(handleLoading());
      });
  };
};

export const clearUserWorkouts = () => {
  return {
    type: CLEAR_USER_WORKOUTS,
  };
};

export const handleClearUserWorkouts = () => {
  return (dispatch) => {
    dispatch(handleHideUserWorkouts());
    dispatch(clearUserWorkouts());
  };
};

export const cancelWorkoutRegistration = (workout) => {
  return (dispatch) => {
    dispatch(handleLoading());
    axiosWithAuth
      .post("/api/cancel-registration", workout)
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleLoading());
        dispatch(handleGetUserWorkouts());
        dispatch(getWorkoutSchedule(workout.workoutType));
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
        dispatch(handleLoading());
      });
  };
};

export const updateCrossfitActive = () => {
  return {
    type: UPDATE_CROSSFIT_ACTIVE,
  };
};

export const updateFitnessActive = () => {
  return {
    type: UPDATE_FITNESS_ACTIVE,
  };
};

export const updateWorkoutType = (workoutType) => {
  return (dispatch) => {
    if (workoutType === "CROSSFIT") {
      dispatch(updateCrossfitActive());
      handleRefreshToken();
    }
    if (workoutType === "FITNESS") {
      dispatch(updateFitnessActive());
      handleRefreshToken();
    }
  };
};

const handleRefreshToken = async () => {
  await axios
    .get("/api/refreshToken", {
      headers: {
        refreshToken: window.localStorage.refreshToken,
      },
    })
    .then((res) => {
      window.localStorage.setItem("accessToken", res.data);
      handleSaveUser(res.data);
    })
    .catch((err) => console.log(err.response.data));
};

axiosWithAuth.interceptors.request.use(
  async (config) => {
    const decoded = jwt.decode(window.localStorage.accessToken);
    if (new Date(decoded.exp * 1000) < new Date()) {
      await handleRefreshToken()
        .then(() => {
          config.headers.accessToken = window.localStorage.accessToken;
          return config;
        })
        .catch((err) => console.log(err));
    }
    config.headers.accessToken = window.localStorage.accessToken;
    return config;
  },
  (err) => {
    console.log(err);
    return Promise.reject(err);
  }
);
// axiosWithAuth.interceptors.response.use(
//   async (res) => {
//     return res;
//   },
//   async (err) => {
//     const originalRequest = err.config;
//     if (err.response.status === 401) {
//       await handleRefreshToken()
//         .then(() => {
//           originalRequest.headers.accessToken = window.localStorage.accessToken;
//           return originalRequest;
//         })
//         .catch((err) => console.log(err));
//     } else {
//       return err.response;
//     }
//   }
// );
