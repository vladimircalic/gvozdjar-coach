import { SET_SERVER_RESPONSE, HIDE_SERVER_MESSAGE } from "./serverResponseTypes";

export const setServerResponse = (response) => {
  return {
    type: SET_SERVER_RESPONSE,
    payload: response,
  };
};

export const hideServerResponse = () => {
  return {
    type: HIDE_SERVER_MESSAGE,
  };
};

export const handleServerResponse = (response) => {
  return (dispatch) => {
    if ((response && response.message) || (response && response.error)) {
      dispatch(setServerResponse(response));
      clearTimeout(window.timer);

      window.timer = setTimeout(() => {
        dispatch(hideServerResponse());
      }, 2000);
    }
  };
};
