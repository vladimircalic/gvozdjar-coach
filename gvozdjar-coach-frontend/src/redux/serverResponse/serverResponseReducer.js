import { SET_SERVER_RESPONSE, HIDE_SERVER_MESSAGE } from "./serverResponseTypes";

const initState = {
  serverResponse: null,
  showServerResponse: false,
};

const serverResponseReducer = (state = initState, action) => {
  switch (action.type) {
    case SET_SERVER_RESPONSE:
      return {
        ...state,
        showServerResponse: true,
        serverResponse: action.payload,
      };
    case HIDE_SERVER_MESSAGE:
      return {
        ...state,
        showServerResponse: false,
      };
    default:
      return state;
  }
};
export default serverResponseReducer;
