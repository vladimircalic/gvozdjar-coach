import {
  GET_WORKOUT_SCHEDULE_REQUEST,
  GET_WORKOUT_SCHEDULE_SUCCESS,
  GET_WORKOUT_SCHEDULE_ERROR,
  GET_FITNESS_SCHEDULE_REQUEST,
  GET_FITNESS_SCHEDULE_SUCCESS,
  GET_FITNESS_SCHEDULE_ERROR,
  GET_CROSSFIT_SCHEDULE_REQUEST,
  GET_CROSSFIT_SCHEDULE_SUCCESS,
  GET_CROSSFIT_SCHEDULE_ERROR,
} from "./scheduleTypes";

const initialState = {
  loading: false,
  crossfitSchedule: [],
  fitnessSchedule: [],
  selectedSchedule: [],
};

const workoutScheduleReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_WORKOUT_SCHEDULE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_WORKOUT_SCHEDULE_SUCCESS:
      return {
        ...state,
        loading: false,
        selectedSchedule: action.payload,
      };
    case GET_WORKOUT_SCHEDULE_ERROR:
      return {
        ...state,
        loading: false,
      };
    case GET_FITNESS_SCHEDULE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_FITNESS_SCHEDULE_SUCCESS:
      return {
        ...state,
        loading: false,
        fitnessSchedule: action.payload,
      };
    case GET_FITNESS_SCHEDULE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case GET_CROSSFIT_SCHEDULE_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case GET_CROSSFIT_SCHEDULE_SUCCESS:
      return {
        ...state,
        loading: false,
        crossfitSchedule: action.payload,
      };
    case GET_CROSSFIT_SCHEDULE_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default workoutScheduleReducer;
