import {
  GET_WORKOUT_SCHEDULE_REQUEST,
  GET_WORKOUT_SCHEDULE_SUCCESS,
  GET_WORKOUT_SCHEDULE_ERROR,
  GET_FITNESS_SCHEDULE_REQUEST,
  GET_FITNESS_SCHEDULE_SUCCESS,
  GET_FITNESS_SCHEDULE_ERROR,
  GET_CROSSFIT_SCHEDULE_REQUEST,
  GET_CROSSFIT_SCHEDULE_SUCCESS,
  GET_CROSSFIT_SCHEDULE_ERROR,
} from "./scheduleTypes";
import axios from "axios";
import { handleServerResponse } from "../serverResponse/serverResponseActions";

export const getWorkoutScheduleRequest = () => {
  return {
    type: GET_WORKOUT_SCHEDULE_REQUEST,
  };
};

export const getWorkoutScheduleSuccess = (results) => {
  return {
    type: GET_WORKOUT_SCHEDULE_SUCCESS,
    payload: results,
  };
};
export const getWorkoutScheduleError = (error) => {
  return {
    type: GET_WORKOUT_SCHEDULE_ERROR,
  };
};

export const getWorkoutSchedule = (type) => {
  return (dispatch) => {
    dispatch(getWorkoutScheduleRequest());
    axios
      .get(`/api/workout-schedule/${type}`)
      .then((res) => {
        dispatch(getWorkoutScheduleSuccess(res.data));
        dispatch(handleServerResponse(res.data));
      })
      .catch((err) => {
        dispatch(getWorkoutScheduleError(err.response.data));
        dispatch(handleServerResponse(err.response.data));
      });
  };
};

export const getFitnessScheduleRequest = () => {
  return {
    type: GET_FITNESS_SCHEDULE_REQUEST,
  };
};

export const getFitnessScheduleSuccess = (results) => {
  return {
    type: GET_FITNESS_SCHEDULE_SUCCESS,
    payload: results,
  };
};
export const getFitnessScheduleError = (error) => {
  return {
    type: GET_FITNESS_SCHEDULE_ERROR,
    payload: error,
  };
};

export const getFitnessSchedule = () => {
  return (dispatch) => {
    dispatch(getFitnessScheduleRequest());
    axios
      .get(`/api/workout-schedule/FITNESS`)
      .then((res) => dispatch(getFitnessScheduleSuccess(res.data)))
      .catch((err) => dispatch(getFitnessScheduleError(err.response.data)));
  };
};
export const getCrossfitScheduleRequest = () => {
  return {
    type: GET_CROSSFIT_SCHEDULE_REQUEST,
  };
};

export const getCrossfitScheduleSuccess = (results) => {
  return {
    type: GET_CROSSFIT_SCHEDULE_SUCCESS,
    payload: results,
  };
};
export const getCrossfitScheduleError = (error) => {
  return {
    type: GET_CROSSFIT_SCHEDULE_ERROR,
    payload: error,
  };
};

export const getCrossfitSchedule = () => {
  return (dispatch) => {
    dispatch(getCrossfitScheduleRequest());
    axios
      .get(`/api/workout-schedule/CROSSFIT`)
      .then((res) => dispatch(getCrossfitScheduleSuccess(res.data)))
      .catch((err) => dispatch(getCrossfitScheduleError(err.response.data)));
  };
};
