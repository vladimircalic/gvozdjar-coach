import {
  HANDLE_ADMIN_IS_LOGGED,
  SET_WORKOUT_ATTENDANTS,
  SET_SELECTED_WORKOUT,
  HANDLE_LOADING,
  SAVE_USERS,
  SET_SELECTED_USER,
} from "./adminTypes";
import { handleServerResponse } from "../serverResponse/serverResponseActions";
import { handleShowWorkoutDetails, handleShowUserDetails } from "../interface/interfaceActions";
import { getWorkoutSchedule } from "../workoutSchedule/scheduleActions";
import axios from "axios";
import history from "../../History";

const axiosWithAuth = axios.create();
const jwt = require("jsonwebtoken");

const handleRefreshToken = async () => {
  await axios
    .get("/api/refreshToken", {
      headers: {
        refreshToken: window.localStorage.adminRefreshToken,
      },
    })
    .then((res) => {
      window.localStorage.setItem("adminAccessToken", res.data);
    })
    .catch((err) => console.log(err.response.data));
};

axiosWithAuth.interceptors.request.use(
  async (config) => {
    const decoded = jwt.decode(window.localStorage.adminAccessToken);
    if (new Date(decoded.exp * 1000) < new Date()) {
      await handleRefreshToken()
        .then(() => {
          config.headers.accessToken = window.localStorage.adminAccessToken;
          return config;
        })
        .catch((err) => console.log(err));
    }
    config.headers.accessToken = window.localStorage.adminAccessToken;
    return config;
  },
  (err) => {
    console.log(err);
    return Promise.reject(err);
  }
);
axiosWithAuth.interceptors.response.use(
  (res) => {
    return res;
  },
  async (err) => {
    const originalRequest = err.config;
    if (err.response.status === 401) {
      await handleRefreshToken()
        .then(() => {
          originalRequest.headers.accessToken = window.localStorage.adminAccessToken;
          return originalRequest;
        })
        .catch((err) => console.log(err));
    } else {
      return err.response;
    }
  }
);

export const handleLoading = () => {
  return {
    type: HANDLE_LOADING,
  };
};

export const handleAdminLogged = () => {
  return {
    type: HANDLE_ADMIN_IS_LOGGED,
  };
};

export const handleAdminLogin = (data) => {
  return (dispatch) => {
    axios
      .post("/api/admin-login", data)
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleAdminLogged());
        window.localStorage.setItem("adminAccessToken", res.data.accessToken);
        window.localStorage.setItem("adminRefreshToken", res.data.refreshToken);
      })
      .catch((err) => dispatch(handleServerResponse(err.response.data)));
  };
};

export const handleAdminLogout = () => {
  return (dispatch) => {
    axios
      .delete("/api/admin-logout", {
        headers: {
          accessToken: window.localStorage.adminAccessToken,
          refreshToken: window.localStorage.adminRefreshToken,
        },
      })
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleAdminLogged());
        localStorage.removeItem("adminAccessToken");
        localStorage.removeItem("adminRefreshToken");
        history.push("/login");
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
      });
  };
};

export const setWorkoutAttendants = (attendants) => {
  return {
    type: SET_WORKOUT_ATTENDANTS,
    payload: attendants,
  };
};

export const setSelectedWorkout = (workout) => {
  return {
    type: SET_SELECTED_WORKOUT,
    payload: workout,
  };
};

export const handleWorkoutInfo = (type, day, c) => {
  return (dispatch) => {
    dispatch(handleLoading());
    dispatch(handleShowWorkoutDetails());
    const workoutInfo = {
      workoutType: type,
      trainingDate: day.date,
      trainingDay: day.day,
      trainingClass: c.time,
    };
    dispatch(setSelectedWorkout({ day: day, class: c }));
    axiosWithAuth
      .post("/api/workout-info", workoutInfo)
      .then((res) => {
        dispatch(setWorkoutAttendants(res.data));
        dispatch(handleLoading());
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
        dispatch(handleLoading());
      });
  };
};

export const handleRemoveAttendant = (id, workoutType) => {
  return (dispatch) => {
    dispatch(handleLoading());
    axiosWithAuth
      .delete(`/api/remove-attendant/${id}`)
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleShowWorkoutDetails());
        dispatch(getWorkoutSchedule(workoutType));
        dispatch(handleLoading());
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
        dispatch(handleShowWorkoutDetails());
        dispatch(getWorkoutSchedule(workoutType));
        dispatch(handleLoading());
      });
  };
};

export const saveUsers = (users) => {
  return {
    type: SAVE_USERS,
    payload: users,
  };
};

export const handleGetUsers = () => {
  return (dispatch) => {
    axiosWithAuth
      .get("/api/all-users")
      .then((res) => {
        dispatch(saveUsers(res.data));
      })
      .catch((err) => console.log(err));
  };
};

export const handleVerifyUser = (id, program) => {
  return (dispatch) => {
    axiosWithAuth
      .put("/api/verify-user", { userId: id, program: program })
      .then((res) => {
        dispatch(handleGetUsers());
      })
      .catch((err) => {
        console.log(err);
      });
  };
};

export const setSelectedUser = (user) => {
  return {
    type: SET_SELECTED_USER,
    payload: user,
  };
};

export const handleUserDetails = (user) => {
  return (dispatch) => {
    dispatch(setSelectedUser(user));
    dispatch(handleShowUserDetails());
  };
};

export const handleDeleteUser = (userId) => {
  return (dispatch) => {
    dispatch(handleLoading());
    axiosWithAuth
      .delete(`/api/delete-user/${userId}`)
      .then((res) => {
        dispatch(handleServerResponse(res.data));
        dispatch(handleShowUserDetails());
        dispatch(handleGetUsers());
        dispatch(handleLoading());
      })
      .catch((err) => {
        dispatch(handleServerResponse(err.response.data));
        dispatch(handleShowUserDetails());
        dispatch(handleLoading());
      });
  };
};
