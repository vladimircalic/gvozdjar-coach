import {
  HANDLE_ADMIN_IS_LOGGED,
  SET_WORKOUT_ATTENDANTS,
  SET_SELECTED_WORKOUT,
  HANDLE_LOADING,
  SAVE_USERS,
  SET_SELECTED_USER,
} from "./adminTypes";

const initialState = {
  adminIsLogged: false,
  selectedWorkout: { day: "", class: "" },
  selectedWorkoutAttendants: [],
  loading: false,
  users: [],
  selectedUser: {},
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case HANDLE_LOADING:
      return {
        ...state,
        loading: !state.loading,
      };
    case HANDLE_ADMIN_IS_LOGGED:
      return {
        ...state,
        adminIsLogged: !state.adminIsLogged,
      };
    case SET_WORKOUT_ATTENDANTS:
      return {
        ...state,
        selectedWorkoutAttendants: action.payload,
      };
    case SET_SELECTED_WORKOUT:
      return {
        ...state,
        selectedWorkout: action.payload,
      };
    case SAVE_USERS:
      return {
        ...state,
        users: action.payload,
      };
    case SET_SELECTED_USER:
      return {
        ...state,
        selectedUser: action.payload,
      };
    default:
      return state;
  }
};

export default adminReducer;
