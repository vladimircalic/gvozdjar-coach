import { combineReducers } from "redux";
import workoutScheduleReducer from "./workoutSchedule/scheduleReducer";
import serverResponseReducer from "./serverResponse/serverResponseReducer";
import userReducer from "./user/userReducer";
import interfaceReducer from "./interface/interfaceReducer";
import adminReducer from "./admin/adminReducer";

const rootReducer = combineReducers({
  workoutSchedule: workoutScheduleReducer,
  serverResponse: serverResponseReducer,
  user: userReducer,
  interface: interfaceReducer,
  admin: adminReducer,
});

export default rootReducer;
