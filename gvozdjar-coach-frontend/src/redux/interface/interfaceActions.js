import { handleClearUserWorkouts } from "../user/userActions";
import {
  SHOW_REGISTER_FORM,
  SHOW_LOGIN_FORM,
  TOGGLE_BACKDROP,
  SHOW_USER_INFO,
  SHOW_SIGN_FOR_WORKOUT,
  SHOW_SIDEBAR,
  CLOSE_SIDEBAR,
  SHOW_IMAGES,
  SHOW_WORKOUT_DETAILS,
  CLOSE_USER_FORMS,
  SHOW_USER_DETAILS,
  SHOW_USER_WORKOUTS,
  HIDE_USER_WORKOUTS,
} from "./interfaceTypes";

export const showRegisterForm = () => {
  return {
    type: SHOW_REGISTER_FORM,
  };
};

export const showLoginForm = () => {
  return {
    type: SHOW_LOGIN_FORM,
  };
};

export const toggleBackdrop = () => {
  return {
    type: TOGGLE_BACKDROP,
  };
};

export const showUserInfo = () => {
  return {
    type: SHOW_USER_INFO,
  };
};

export const closeUserForms = () => {
  return {
    type: CLOSE_USER_FORMS,
  };
};

export const handleShowUserInfo = () => {
  return (dispatch) => {
    dispatch(toggleBackdrop());
    dispatch(showUserInfo());
  };
};

export const closeUserInfo = () => {
  return (dispatch) => {
    dispatch(toggleBackdrop());
    dispatch(showUserInfo());
    dispatch(closeUserForms());
    dispatch(handleClearUserWorkouts());
  };
};

export const showSignForWorkout = () => {
  return {
    type: SHOW_SIGN_FOR_WORKOUT,
  };
};

export const handleShowSignForWorkout = () => {
  return (dispatch) => {
    dispatch(toggleBackdrop());
    dispatch(showSignForWorkout());
  };
};

export const handleShowSidebar = () => {
  return {
    type: SHOW_SIDEBAR,
  };
};

export const closeSidebar = () => {
  return {
    type: CLOSE_SIDEBAR,
  };
};

export const showImages = () => {
  return {
    type: SHOW_IMAGES,
  };
};

export const handleShowImages = () => {
  return (dispatch) => {
    dispatch(toggleBackdrop());
    dispatch(showImages());
  };
};

export const handleShowWorkoutDetails = () => {
  return {
    type: SHOW_WORKOUT_DETAILS,
  };
};

export const handleShowUserDetails = () => {
  return {
    type: SHOW_USER_DETAILS,
  };
};

export const handleShowUserWorkouts = () => {
  return {
    type: SHOW_USER_WORKOUTS,
  };
};

export const handleHideUserWorkouts = () => {
  return {
    type: HIDE_USER_WORKOUTS,
  };
};
