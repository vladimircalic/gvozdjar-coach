import {
  SHOW_REGISTER_FORM,
  SHOW_LOGIN_FORM,
  TOGGLE_BACKDROP,
  SHOW_USER_INFO,
  SHOW_SIGN_FOR_WORKOUT,
  SHOW_SIDEBAR,
  CLOSE_SIDEBAR,
  SHOW_IMAGES,
  SHOW_WORKOUT_DETAILS,
  CLOSE_USER_FORMS,
  SHOW_USER_DETAILS,
  SHOW_USER_WORKOUTS,
  HIDE_USER_WORKOUTS,
} from "./interfaceTypes";

const initialState = {
  showRegisterForm: false,
  showLoginForm: false,
  showBackdrop: false,
  showUserInfo: false,
  showSignForWorkout: false,
  showSidebar: false,
  showImages: false,
  showWorkoutDetails: false,
  showUserDetails: false,
  showUserWorkouts: false,
};

const interfaceReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_REGISTER_FORM:
      return {
        ...state,
        showLoginForm: false,
        showRegisterForm: true,
      };
    case SHOW_LOGIN_FORM:
      return {
        ...state,
        showLoginForm: !state.showLoginForm,
        showRegisterForm: false,
      };
    case CLOSE_USER_FORMS:
      return {
        ...state,
        showLoginForm: false,
        showRegisterForm: false,
      };
    case TOGGLE_BACKDROP:
      return {
        ...state,
        showBackdrop: !state.showBackdrop,
      };
    case SHOW_USER_INFO:
      return {
        ...state,
        showLoginForm: true,
        showRegisterForm: false,
        showUserInfo: !state.showUserInfo,
      };
    case SHOW_SIGN_FOR_WORKOUT:
      return {
        ...state,
        showSignForWorkout: !state.showSignForWorkout,
      };
    case SHOW_SIDEBAR:
      return {
        ...state,
        showSidebar: !state.showSidebar,
      };
    case CLOSE_SIDEBAR:
      return {
        ...state,
        showSidebar: false,
      };
    case SHOW_IMAGES:
      return {
        ...state,
        showImages: !state.showImages,
      };
    case SHOW_WORKOUT_DETAILS:
      return {
        ...state,
        showWorkoutDetails: !state.showWorkoutDetails,
      };
    case SHOW_USER_DETAILS:
      return {
        ...state,
        showUserDetails: !state.showUserDetails,
      };
    case SHOW_USER_WORKOUTS:
      return {
        ...state,
        showUserWorkouts: true,
      };
    case HIDE_USER_WORKOUTS:
      return {
        ...state,
        showUserWorkouts: false,
      };
    default:
      return state;
  }
};

export default interfaceReducer;
