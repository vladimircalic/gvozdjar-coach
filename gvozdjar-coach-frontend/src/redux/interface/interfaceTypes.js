export const SHOW_REGISTER_FORM = "SHOW_REGISTER_FORM";
export const SHOW_LOGIN_FORM = "SHOW_LOGIN_FORM";
export const TOGGLE_BACKDROP = "TOGGLE_BACKDROP";
export const SHOW_USER_INFO = "SHOW_USER_INFO";
export const SHOW_SIGN_FOR_WORKOUT = "SHOW_SIGN_FOR_WORKOUT";
export const SHOW_SIDEBAR = "SHOW_SIDEBAR";
export const CLOSE_SIDEBAR = "CLOSE_SIDEBAR";
export const SHOW_IMAGES = "SHOW_IMAGES";
export const SHOW_WORKOUT_DETAILS = "SHOW_WORKOUT_DETAILS";
export const CLOSE_USER_FORMS = "CLOSE_USER_FORMS";
export const SHOW_USER_DETAILS = "SHOW_USER_DETAILS";
export const SHOW_USER_WORKOUTS = "SHOW_USER_WORKOUTS";
export const HIDE_USER_WORKOUTS = "HIDE_USER_WORKOUTS";
